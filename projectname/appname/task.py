from __future__ import absolute_import
from celery import shared_task
from django.db import transaction
from .models import AccountInfo,TopUp, Transaction





@shared_task
@transaction.atomic()
def process_transfer(info1):
    savepoint1 = transaction.savepoint()
    process_status=1
    try:
        # update transferfromusername account balance
        # info = [transferfromusername, transfertousername, transferamount, transfertime]
        getinfo1 = list(AccountInfo.objects.filter(username=info1[0]))
        getbalance1 = float(getinfo1[0].balance)
        gettransferamount1 = float(info1[2])
        updatebalance1 = getbalance1 - gettransferamount1
        AccountInfo.objects.filter(username=info1[0]).update(balance=updatebalance1)
    except Exception:
        process_status=0

    try:
        # update transfertousername account balance
        getinfo2 = list(AccountInfo.objects.filter(username=info1[1]))
        getbalance2 = float(getinfo2[0].balance)
        gettransferamount2 = float(info1[2])
        updatebalance2 = getbalance2 + gettransferamount2
        AccountInfo.objects.filter(username=info1[1]).update(balance=updatebalance2)
    except Exception:
        process_status=0
    try:
        # update transfer process to success
        Transaction.objects.filter(transaction_from_username = info1[0], transaction_to_username = info1[1], transaction_amount = info1[2],
                                   transaction_time = info1[3]).update(transaction_status = "Success")
    except Exception:
        process_status=0

    if process_status==1 :
        transaction.savepoint_commit(savepoint1)
    else:
        # if failed, roll back the transaction and update the transfer process as failed
        transaction.savepoint_rollback(savepoint1)
        Transaction.objects.filter(transaction_from_username=info1[0], transaction_to_username=info1[1],
                                   transaction_amount=info1[2], transaction_time=info1[3]).update(transaction_status="Failed")

    return info1

@shared_task
@transaction.atomic()
def process_topup(info2):
    savepoint2 = transaction.savepoint()
    process_status2 = 1

    try:
        # update topupusername account balance
        # info = [topupusername, topupamount, gettopuptime]
        getinfo3 = list(AccountInfo.objects.filter(username=info2[0]))
        getbalance3 = float(getinfo3[0].balance)
        gettopupamount3 = float(info2[1])
        updatebalance3 = getbalance3 + gettopupamount3
        AccountInfo.objects.filter(username=info2[0]).update(balance=updatebalance3)
    except Exception:
        process_status2 = 0

    try:
        # update the topuyp process and account balance
        TopUp.objects.filter(top_up_username=info2[0], top_up_amount=info2[1], top_up_time=info2[2]).update(
            top_up_status="Success", top_up_account_balance=updatebalance3)
    except Exception:
        process_status2 = 0



    if process_status2==1 :
        transaction.savepoint_commit(savepoint2)
    else:
        # if failed, roll back the transaction and update the top up process as failed
        transaction.savepoint_rollback(savepoint2)
        TopUp.objects.filter(top_up_username=info2[0], top_up_amount=info2[1], top_up_time=info2[2]).update(
            top_up_status="Failed")
    return info2