from __future__ import unicode_literals
from datetime import datetime
from django.core.validators import MinValueValidator
from django.db import models


STATUS_CHOICES = (
    ('processing','PROCESSING'),
    ('successful','SUCCESSFUL'),
    ('failed','FAILED'),

)
# Create your models here.
class AccountInfo(models.Model):
    username        = models.CharField(max_length=30, primary_key=True)
    password        = models.TextField()
    balance         = models.DecimalField(decimal_places=2, max_digits=12, default=1000, validators=[MinValueValidator(0.01)])

    def __str__(self):
        return self.username

class Transaction(models.Model):
    transaction_from_username   = models.ForeignKey(AccountInfo, related_name="+")
    transaction_to_username     = models.ForeignKey(AccountInfo, related_name="+")
    transaction_amount          = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(0.01)])
    transaction_time            = models.DateTimeField(default=datetime.now())
    transaction_status          = models.CharField('Transaction Status *Processing/Successful/Failed ', max_length=30, choices=STATUS_CHOICES, default='processing')

class TopUp(models.Model):
    top_up_username = models.ForeignKey( AccountInfo, related_name="+")
    top_up_amount = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(0.01)])
    top_up_time = models.DateTimeField(default=datetime.now())
    # top_up_time = models.TextField()
    top_up_status = models.CharField('Top up status *Processing/Successful/Failed',max_length=30, choices=STATUS_CHOICES, default='processing')
    top_up_account_balance = models.DecimalField(decimal_places=2, max_digits=12, null=True, validators=[MinValueValidator(0.01)])

