from django.conf.urls import url
from . import views

urlpatterns = [
    url('index/', views.index, name='index'),
    url('signup/', views.signup, name='signup'),
    url('login/', views.login, name='login'),
    url('logout/', views.logout, name='logout'),
    url('home/', views.home, name='home'),
    url('transfer/', views.transfer, name='transfer'),
    url('transferhistory/', views.transferhistory, name='transferhistory'),
    url('topup/', views.topup, name='topup'),
    url('topuphistory/', views.topuphistory, name='topuphistory'),
    url('', views.index, name='index'),

]
