# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .decorator import is_activate
from .models import AccountInfo, TopUp, Transaction
from django.contrib import messages
from datetime import datetime
from sets import Set
import base64
from .task import process_topup, process_transfer
from django.db.models import Q
from django.db import transaction


# Create your views here.

def index(request):
    return render(request, 'index.html', {'index': index})

def signup(request):
    if request.method == "POST":
        username    = request.POST.get('newusername')
        password    = request.POST.get('newpassword')

        #check this username have been registered
        existuser = list(AccountInfo.objects.filter(username=username).values())
        if existuser:
            messages.add_message(request, messages.INFO, 'The user name already exist, please try another one.')
            return render(request, 'signup.html')
        else:
            #This username havent register, check the length of username
            if len(username) <6 or len(username) > 30:
                messages.add_message(request, messages.INFO, 'Choose a username with 6-30 characters long, any combination that contained at least 1 LOWERCASE letter, 1 UPPERCASE leter and numbers.')
                return render(request, 'signup.html')
            else:
                #This username havent register, username length available, check the format of username
                allowed_characters = Set('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
                if Set(username).issubset(allowed_characters):
                    allowed_number = Set('0123456789')
                    allowed_uppercase = Set('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                    allowed_lowercase = Set('abcdefghijklmnopqrstuvwxyz')
                    allowed_mix_1 = Set('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                    allowed_mix_2 = Set('0123456789abcdefghijklmnopqrstuvwxyz')
                    allowed_mix_3 = Set('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
                    # if username dont contains other special characters, check wether mixed combination of at least one uppercase, one lowercase and number.
                    if Set(username).issubset(allowed_number) or Set(username).issubset(allowed_uppercase) or Set(username).issubset(allowed_lowercase) or Set(username).issubset(allowed_mix_1) or Set(username).issubset(allowed_mix_2) or Set(username).issubset(allowed_mix_3):
                        messages.add_message(request, messages.INFO,'Choose a username with 6-30 characters long, any combination that contained at least 1 LOWERCASE letter, 1 UPPERCASE leter and numbers.')
                        return render(request, 'signup.html')
                    else:
                        #username is available to use , check the password format
                        if len(password) < 6 or len(password) > 30:
                            messages.add_message(request, messages.INFO,
                                                 'Choose a password with 6-30 characters long, any combination that contained at least 1 LOWERCASE letter, 1 UPPERCASE leter and numbers.')
                            return render(request, 'signup.html')
                        else:
                            # This password havent register, password length available, check the format of password = mixed combination of at least one uppercase, one lowercase and number.
                            if Set(password).issubset(allowed_characters):
                                if Set(password).issubset(allowed_number) or Set(password).issubset(allowed_uppercase) or Set(password).issubset(allowed_lowercase) or Set(password).issubset(allowed_mix_1) or Set(password).issubset(allowed_mix_2) or Set(password).issubset(allowed_mix_3):
                                    messages.add_message(request, messages.INFO,'Choose a password with 6-30 characters long, any combination that contained at least 1 LOWERCASE letter, 1 UPPERCASE leter and numbers.')
                                    return render(request, 'signup.html')
                                else:
                                    encrypt_password = base64.b64encode(password)
                                    savepoint = transaction.savepoint()
                                    process_status = 1
                                    try:
                                        AccountInfo.objects.create(username=username, password=encrypt_password, balance=1000)
                                    except Exception:
                                        process_status = 0

                                    if process_status == 1:
                                        transaction.savepoint_commit(savepoint)
                                        return HttpResponseRedirect('../index/')
                                    else:
                                        transaction.savepoint_rollback(savepoint)
                                        messages.add_message(request, messages.INFO,'Sign up process failed, please try again')
                                        return render(request, 'signup.html')

                            else:
                                messages.add_message(request, messages.INFO,'Choose a password with 6-30 characters long, any combination that contained at least 1 LOWERCASE letter, 1 UPPERCASE leter and numbers.')
                                return render(request, 'signup.html')
                else:
                    messages.add_message(request, messages.INFO,'Choose a username with 6-30 characters long, any combination that contained at least 1 LOWERCASE letter, 1 UPPERCASE leter and numbers.')
                    return render(request, 'signup.html')
    else:
        # return HttpResponse(loader.get_template('signup.html').render({'signup': signup}, request))
        return render(request, 'signup.html', {'signup': signup})

def login(request):
    if request.method == "POST":
        username    = request.POST.get('loginusername')
        password    = request.POST.get('loginpassword')

        #check this username have been registered
        existuser = list(AccountInfo.objects.filter(username=username).values())
        if existuser:
            # check wether the password match with login password
            getinfo = list(AccountInfo.objects.filter(username=username))
            checkpassword = getinfo[0].password
            encrypt_login_password = base64.b64encode(password)
            # decrypt_password = base64.b64decode(checkpassword)
            if encrypt_login_password == checkpassword:
                # AccountInfo.objects.filter(username = username).update(login_status = 1)
                request.session['username'] = username

                # update last activate time
                lastactivatedtime = datetime.now()
                request.session['lastactivatedtime'] = str(lastactivatedtime)
                return HttpResponseRedirect('../home/',request)

            else:
                messages.add_message(request, messages.INFO, 'The password incorrect, please try another one.')
                return render(request, 'login.html')
        else:
            messages.add_message(request, messages.INFO, 'The user name dont exist, please try another one.')
            return render(request, 'login.html')
    else:
        return render(request, 'login.html', {'login': login})

def logout(request):
    # remove the session for username
    del request.session['username']
    return HttpResponseRedirect('../index/')

@is_activate
def home(request):
    username = request.session.get('username')
    messages.add_message(request, messages.INFO, "Welcome, " + username)
    getaccountinfo = AccountInfo.objects.filter(username=username)
    args = {'getaccountinfo': getaccountinfo}
    return render(request, 'home.html', args)

@is_activate
def transfer(request):
    if request.method == "POST":
        transferfromusername    = request.session.get('username')
        transfertousername = request.POST.get('transfertousername')
        transferamount    = request.POST.get('transferamount')
        transfertime = datetime.now()

        # check if username available to transfer
        if transferfromusername == transfertousername:
            messages.add_message(request, messages.INFO, 'Transaction failed. Transfer To Username cannot same as Transfer from username.')
            return render(request, 'transfer.html')
        else:
            # check whether transfer to username have been registered
            existuser = list(AccountInfo.objects.filter(username=transfertousername).values())
            if existuser:
                getinfo = list(AccountInfo.objects.filter(username=transferfromusername))
                getbalance = float(getinfo[0].balance)
                gettransferamount = float(transferamount)

                # check whether the transfer amount within 1-2500, with 2 decimal places
                if gettransferamount <= 2500 and gettransferamount >= 1:
                    getdecimalpart = gettransferamount * 100
                    getdecimalparttransferamount = getdecimalpart % 1
                    if getdecimalparttransferamount == 0.0:
                        if getbalance >= gettransferamount:
                            savepoint1 = transaction.savepoint()
                            process_status = 1
                            try:
                                # update transaction history with process status='in process'
                                inserttransfer = Transaction(transaction_from_username_id=transferfromusername,
                                                         transaction_to_username_id=transfertousername,
                                                         transaction_amount=transferamount, transaction_time=transfertime,
                                                         transaction_status="In Process")
                                inserttransfer.save()
                            except Exception:
                                process_status = 0

                            if process_status == 1:
                                transaction.savepoint_commit(savepoint1)
                                info1 = [transferfromusername, transfertousername, transferamount, transfertime]

                                # celery part
                                process_transfer.s(info1).apply_async(countdown=5)
                                return HttpResponseRedirect('../transferhistory/')
                            else:
                                transaction.savepoint_rollback(savepoint1)
                                messages.add_message(request, messages.INFO,'Transaction failed. Please try again.')
                                return render(request, 'transfer.html')

                        else:
                            messages.add_message(request, messages.INFO,'Transaction failed. Not enough amount to transfer.')
                            return render(request, 'transfer.html')
                    else:
                        messages.add_message(request, messages.INFO, 'Transaction failed. Transaction amount must be 2 decimal place.')
                        return render(request, 'transfer.html')
                else:
                    messages.add_message(request, messages.INFO,'Transaction failed. Transfer amount limit is 1 to 2500.')
                    return render(request, 'transfer.html')
            else:
                messages.add_message(request, messages.INFO, 'Transaction failed. The user name dont exist, please try another one.')
                return render(request, 'transfer.html')

    else:
        return render(request, 'transfer.html', {'transfer': transfer})

@is_activate
def transferhistory(request):
    transferusername = request.session.get('username')
    gettransferhistory = Transaction.objects.filter(Q(transaction_from_username=transferusername) | Q(transaction_to_username=transferusername))
    args = {'gettransferhistory': gettransferhistory}


    messages.add_message(request, messages.INFO, "Welcome, " + transferusername)
    return render(request, 'transferhistory.html', args)

@is_activate
@transaction.atomic()
def topup(request):
    if request.method == "POST":
        topupusername    = request.session.get('username')
        topupamount    = request.POST.get('topupamount')
        topuptime = datetime.now()

        # check whether the top up amount within 1-2500, with 2 decimal places
        gettopupamount = float(topupamount)
        if gettopupamount <= 2500 and gettopupamount >= 1:
            getdecimalpart = gettopupamount *100
            getdecimalparttopupamount = getdecimalpart %1
            if getdecimalparttopupamount == 0.0:
                savepoint2 = transaction.savepoint()
                process_status = 1
                try:
                    # update top up history with process status='in process'
                    inserttopup = TopUp(top_up_username_id = topupusername,top_up_amount = topupamount, top_up_time= topuptime, top_up_status = "In Process")
                    inserttopup.save()
                except Exception:
                    process_status = 0

                if process_status == 1:
                    transaction.savepoint_commit(savepoint2)
                    info2 = [topupusername, topupamount, topuptime]

                    # celery part
                    # process_topup.delay(info)
                    process_topup.s(info2).apply_async(countdown=5)
                    return HttpResponseRedirect('../topuphistory/')
                else:
                    transaction.savepoint_rollback(savepoint2)
                    messages.add_message(request, messages.INFO,'Top Up failed. Please try again.')
                    return render(request, 'topup.html')
            else:
                messages.add_message(request, messages.INFO, 'Top Up failed. Top Up amount must be 2 decimal place.')
                return render(request, 'topup.html')
        else:
            messages.add_message(request, messages.INFO, 'Top Up failed. Top Up amount limit is 1 to 2500.')
            return render(request, 'topup.html')
    else:
        return render(request, 'topup.html', {'topup': topup})

@is_activate
def topuphistory(request):
    topupusername = request.session.get('username')
    gettopuphistory = TopUp.objects.filter(top_up_username_id=topupusername)
    args = {'gettopuphistory': gettopuphistory}
    messages.add_message(request, messages.INFO, "Welcome, " + topupusername)
    return render(request, 'topuphistory.html', args)


