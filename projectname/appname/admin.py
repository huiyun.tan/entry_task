# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import AccountInfo, Transaction, TopUp

from django.contrib import admin

# Register your models here.
admin.site.register(AccountInfo)
admin.site.register(Transaction)
admin.site.register(TopUp)