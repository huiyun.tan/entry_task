from datetime import datetime
from .models import AccountInfo
from django.http import HttpResponseRedirect
from django.contrib import messages

def is_activate(function):
    def check_activate_status(request):
        username = request.session.get('username')
        existuser = list(AccountInfo.objects.filter(username=username).values())
        # check exist username
        if existuser:
            lastactivatedtime = request.session.get('lastactivatedtime')
            # lastactivatedtime = "2019-09-02 23:58:38.5555"
            getlastactivatedtime = datetime.strptime(lastactivatedtime, "%Y-%m-%d %H:%M:%S.%f")
            currenttime = datetime.now()
            # setcurrenttime = "2019-09-03 00:07:38.5555"
            # currenttime = datetime.strptime(setcurrenttime, "%Y-%m-%d %H:%M:%S.%f")

            # compare current time and last activated time based on date and hour
            if (currenttime.date() == getlastactivatedtime.date() and currenttime.hour == getlastactivatedtime.hour):
                currenttime2 = currenttime.time().minute
                getlastactivatetime2 = getlastactivatedtime.time().minute
                different2 = currenttime2 - getlastactivatetime2

                # compare current time and last activated time based on miunute, if more than 5 min auto logout
                if (different2 <=5):

                    request.session['lastactivatedtime'] = str(currenttime)
                    return function(request)
                else:
                    messages.add_message(request, messages.INFO, "* Time expired. Please login again")
                    return HttpResponseRedirect('../')

            elif (currenttime.date() == getlastactivatedtime.date()):
                currenttime3 = currenttime.time().hour
                getlastactivatetime3 = getlastactivatedtime.time().hour
                different3 = currenttime3 - getlastactivatetime3
                if (different3 <= 1):
                    currenttime4 = currenttime.time().minute
                    getlastactivatetime4 = getlastactivatedtime.time().minute
                    different4 = currenttime4 + 60 - getlastactivatetime4
                    if (different4 <=5):
                        request.session['lastactivatedtime'] = str(currenttime)
                        return function(request)
                    else:
                        messages.add_message(request, messages.INFO, "* Time expired. Please login again")
                        return HttpResponseRedirect('../logout/')
                else:
                    messages.add_message(request, messages.INFO, "* Time expired. Please login again")
                    return HttpResponseRedirect('../logout/')
            else:
                currenttime5 = currenttime.date().day
                getlastactivatedtime5 = getlastactivatedtime.date().day
                different5 = currenttime5 - getlastactivatedtime5
                if (different5 <= 1):
                    currenttime6 = currenttime.time().hour
                    getlastactivatedtime6 = getlastactivatedtime.time().hour
                    if (currenttime6 == 0 and getlastactivatedtime6 == 23):
                        currenttime7 = currenttime.time().minute
                        getlastactivatedtime7 = getlastactivatedtime.time().minute
                        different7 = currenttime7 + 60 - getlastactivatedtime7
                        if (different7 <=5):
                            request.session['lastactivatedtime'] = str(currenttime)
                            return function(request)
                        else:
                            messages.add_message(request, messages.INFO, "* Time expired. Please login again")
                            return HttpResponseRedirect('../logout/')
                    else:
                        messages.add_message(request, messages.INFO, "* Time expired. Please login again")
                        return HttpResponseRedirect('../logout/')
                else:
                    messages.add_message(request, messages.INFO, "* Time expired. Please login again")
                    return HttpResponseRedirect('../logout/')
        else:
            return HttpResponseRedirect('../')

    return check_activate_status
