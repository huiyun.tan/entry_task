# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-05 10:56
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appname', '0007_auto_20190905_1855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topup',
            name='top_up_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 5, 18, 56, 3, 64528)),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 5, 18, 56, 3, 64058)),
        ),
    ]
