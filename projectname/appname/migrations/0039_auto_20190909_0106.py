# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-08 17:06
from __future__ import unicode_literals

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('appname', '0038_auto_20190909_0047'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionAccountBalanceUpdate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('transaction_account_balance', models.DecimalField(decimal_places=2, max_digits=12, null=True, validators=[django.core.validators.MinValueValidator(0.01)])),
                ('transaction_username', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='appname.AccountInfo')),
            ],
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='transaction_from_account_balance',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='transaction_to_account_balance',
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 9, 1, 6, 57, 447186)),
        ),
    ]
