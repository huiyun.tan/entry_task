# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-09 10:52
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appname', '0053_auto_20190909_1847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='transaction_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 9, 18, 52, 47, 928940)),
        ),
    ]
