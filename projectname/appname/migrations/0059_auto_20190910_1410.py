# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-10 06:10
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appname', '0058_auto_20190910_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topup',
            name='top_up_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 10, 14, 10, 33, 360201)),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 10, 14, 10, 33, 359729)),
        ),
    ]
