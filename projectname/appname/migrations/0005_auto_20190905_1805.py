# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-05 10:05
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appname', '0004_auto_20190905_1101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topup',
            name='top_up_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 5, 18, 5, 37, 702981)),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='transaction_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 5, 18, 5, 37, 702516)),
        ),
    ]
