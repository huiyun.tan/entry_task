# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-09-09 09:44
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appname', '0051_auto_20190909_1743'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='transaction_time',
            field=models.DateTimeField(default=datetime.datetime(2019, 9, 9, 17, 44, 49, 264141)),
        ),
    ]
